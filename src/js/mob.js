'use strict'

export class Mob {
  constructor(name, pv, force, init, dg,armor) {

    this.name = name;
    this.pv = pv;
    this.force = force ;
    this.init = init;
    this.dg = dg;
    this.armor = armor;
    this.boss = false;
  }

  mobDeath() {
    if (this.pv <= 0) {
      console.log(`you have killed this ${this.name} !`);
      
    }
  }
}
